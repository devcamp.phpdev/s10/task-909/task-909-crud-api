<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OfficesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    private $offices;
    public function __construct(){
        $vPath = dirname(dirname(__DIR__)) . "\data\offices.json";
        $vData = file_get_contents($vPath);
        //die ($vData);
        $this->offices = json_decode($vData, true);
    }
    public function index()
    {
        return response()->json($this->offices);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $id = 0;
        if (count($this->offices) == 0){
            $id = 1;
        } else {
            $lastElement = end($this->offices);
            $id = $lastElement["id"] + 1;
        }
        $newOffice  =[
            "id" => $id,
            "address" => $request->address ?? "",
            "email" => $request->email ??"",
            "phone" => $request->phone ??""
        ];
        $this->offices[] = $newOffice;
        $this->saveData();
        return response()->json($newOffice, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (count ($this->offices) > 0){
            foreach ($this->offices as $office){
                if ($office["id"] == $id){
                    return response()->json($office);
                }
            }
        }
        return response()->json(["Errorr"=>"Office not found"], 404);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $found = false;
        if (count ($this->offices) > 0){
            foreach ($this->offices as $key=>$office){
                if ($office["id"] == $id){
                    $office["address"] = $request->address ??"";
                    $office["email"] = $request->email ??"";
                    $office["phone"] = $request->phone??"";
                    $this->offices[$key] = $office;
                    $this->saveData();
                    $found = true;
                    return response()->json($office);
                }
            }
        }
        if ($found == false){
            return response()->json(["Errorr"=>"Office not found"], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (count ($this->offices) > 0){
            foreach ($this->offices as $key=>$office){
                if ($office["id"] == $id){
                    unset($this->offices[$key]);
                    $this->saveData();
                    return response()->json(null, 204);
                }
            }
        }
        return response()->json(["Errorr"=>"Office not found"], 404);
    }

    private function saveData(){
        $data = json_encode($this->offices);
        $vPath = dirname(dirname(__DIR__)) . "\data\offices.json";
        $vData = file_put_contents($vPath, $data);
    }
}
