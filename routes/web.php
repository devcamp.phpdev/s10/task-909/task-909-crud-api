<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::redirect('/welcome', '/home');

Route::view('/home','home');
Route::view('/about','about')->name('about');
Route::post('/post', function(){
    return view('post');
});
Route::put('/put', function(){
    return view('put');
});
Route::post('/delete', function(){
    return view('delete');
});

Route::match(['get','post'],'/contactus', function(){
    return view('contactus');
});

Route::any('/sampleallmethod', function(){
    return view('sampleallmethod');
});